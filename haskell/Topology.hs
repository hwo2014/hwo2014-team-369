module Topology where

import GameInitModel

import Data.List
import Data.Maybe
-- used to check local lens-family-core package
--import Lens.Family

data Segment = Rectiliniu | Curbat deriving (Eq, Show)

topologie :: [Segment]
topologie = [ Rectiliniu
            , Rectiliniu
            , Rectiliniu
            , Rectiliniu
            , Curbat
            , Curbat
            , Curbat
            , Curbat
            , Curbat
            , Rectiliniu
            , Rectiliniu
            , Curbat
            , Rectiliniu
            , Rectiliniu
            , Curbat
            , Curbat
            , Curbat
            , Curbat
            , Rectiliniu
            , Curbat
            , Curbat
            , Curbat
            , Curbat
            , Curbat
            , Curbat
            , Rectiliniu
            , Curbat
            , Curbat
            , Rectiliniu
            , Curbat
            , Curbat
            , Curbat
            , Curbat
            , Curbat
            , Curbat
            , Rectiliniu
            , Rectiliniu
            , Rectiliniu
            , Rectiliniu
            , Rectiliniu
            ]

pieceToSegment :: Piece -> Segment
pieceToSegment p = case pieceLength p of
  Nothing -> Curbat
  Just _  -> Rectiliniu

makeTopo :: Track -> [Segment]
makeTopo = map pieceToSegment . pieces

calibrate :: [(Int,Segment)] -> Int -> [(Int,Segment)]
calibrate ss i = head dropped : takeWhile different (tail dropped)
  where
  dropped = dropWhile different $ ss ++ ss
  different = \ (j,_) -> j /= i

-- TODO: make a decision model constructed as a Map
-- TODO: this is where most of the processing will go

topo_length :: Int
topo_length = length topologie

topo_decorated_with_indexes :: [(Int,Segment)]
topo_decorated_with_indexes = zip [0..] topologie

topo_decorated_with_indexes_zipped_with_its_tail :: [((Int,Segment),(Int,Segment))]
topo_decorated_with_indexes_zipped_with_its_tail =
  zip topo_decorated_with_indexes
      (tail topo_decorated_with_indexes)

is_there_a_change :: ((Int,Segment),(Int,Segment)) -> Bool
is_there_a_change tuple_of_tuples =
  (snd . fst $ tuple_of_tuples) /= (snd . snd $ tuple_of_tuples)

how_many_to_drop :: Int
how_many_to_drop =
  fst . snd . fromJust . (find is_there_a_change) $
    topo_decorated_with_indexes_zipped_with_its_tail

calibrated_topo :: [(Int, Segment)]
calibrated_topo =
  (take topo_length) . (drop how_many_to_drop) $
    topo_decorated_with_indexes ++ topo_decorated_with_indexes

