module GameState where

import GameInitModel
import CarPositionsModel

data GameState = GameState
  { gameInitData  :: GameInitData
  , myCar         :: CarId
  , carPositions  :: [CarPosition]
  , gameTick      :: Int
  , speed         :: Float
  } deriving Show

data SpeedInfo = MkSpeedInfo
  { previous_game_tick :: Int
  , current_game_tick  :: Int
  , previous_piece_index :: Int
  , current_piece_index  :: Int
  , previous_in_piece_distance :: Float
  , current_in_piece_distance  :: Float
  , previous_speed :: Float
  }

mkSpeedInfo :: [CarPosition] -> Int -> GameState -> SpeedInfo
mkSpeedInfo cps tick oldgs = MkSpeedInfo
  (gameTick oldgs) tick
  (getIndex . getCar $ carPositions oldgs) (getIndex $ getCar cps)
  (getDistance . getCar $ carPositions oldgs) (getDistance $ getCar cps)
  (speed oldgs)
  where
  getCar cs = case findCar (color . myCar $ oldgs) cs of
    Nothing -> error "My car is not on the track!"
    Just cp -> cp
  getIndex = pieceIndex . piecePosition
  getDistance = inPieceDistance . piecePosition

current_speed :: SpeedInfo -> Float
current_speed sp_info =
  if we_are_in_the_same_piece && game_tick_has_advanced
  then
    ( current_in_piece_distance sp_info - previous_in_piece_distance sp_info ) / fromIntegral game_tick_advancement
  else
    previous_speed sp_info
  where
    we_are_in_the_same_piece = current_piece_index sp_info == previous_piece_index sp_info
    game_tick_advancement = current_game_tick sp_info - previous_game_tick sp_info
    game_tick_has_advanced = game_tick_advancement > 0

undefinedGameState :: GameState
undefinedGameState = GameState undefined undefined undefined undefined undefined

