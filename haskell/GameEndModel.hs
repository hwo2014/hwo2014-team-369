{-# LANGUAGE OverloadedStrings #-}

module GameEndModel where

import GameInitModel(CarId(..))

import Data.Aeson ((.:), (.:?), object, FromJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>))

import Data.List

-- LapResult
data LapResult = LapResult {
  laps :: Maybe Int,
  ticks :: Maybe Int,
  millis :: Maybe Int
} deriving (Show)

instance FromJSON LapResult where
  parseJSON (Object v) =
    LapResult <$>
    (v .:? "laps") <*>
    (v .:? "ticks") <*>
    (v .:? "millis")

-- GameEndResult

data GameEndResult = GameEndResult {
  car :: CarId,
  result :: LapResult
} deriving (Show)

instance FromJSON GameEndResult where
  parseJSON (Object v) =
    GameEndResult <$>
    (v .: "car") <*>
    (v .: "result")

-- GameEndData

data GameEndData = GameEndData {
  results :: [GameEndResult],
  bestLaps :: [GameEndResult]
} deriving (Show)

instance FromJSON GameEndData where
  parseJSON (Object v) =
    GameEndData <$>
    (v .: "results") <*>
    (v .: "bestLaps")

