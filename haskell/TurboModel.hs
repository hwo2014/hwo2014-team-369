{-# LANGUAGE OverloadedStrings #-}

module TurboModel where

import Data.Aeson ((.:), object, FromJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>))

--- TurboData

data TurboData = TurboData {
  turboDurMillis :: Float,
  turboDurTicks :: Int,
  turboFactor :: Float
} deriving (Show)

instance FromJSON TurboData where
  parseJSON (Object v) =
    TurboData <$>
    (v .: "turboDurationMilliseconds") <*>
    (v .: "turboDurationTicks") <*>
    (v .: "turboFactor")

