{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network (connectTo, PortID(..))
import Network.Socket (HostName)
import System.IO (hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.List
import Data.IORef (IORef, readIORef, writeIORef, newIORef)
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson (decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), (.:?), Result(..))

import GameInitModel
import GameEndModel
import CarPositionsModel
import TurboModel
import GameState

type ClientMessage = String

messageJoin botname botkey = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"
messageThrottle amount = "{\"msgType\":\"throttle\",\"data\":" ++ (show amount) ++ "}"
messageSwitchLane direction = "{\"msgType\":\"switchLane\",\"data\":" ++ (show direction) ++ "}"
messagePing = "{\"msgType\":\"ping\",\"data\":{}}"
messageCreateRace botname botkey trackName = "{\"msgType\":\"createRace\",\"data\":{\"botId\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"},\"trackName\":\"" ++ trackName ++ "\",\"password\":\"flamboyant\",\"carCount\":1}}"
messageTurbo taunt = "{\"msgType\":\"turbo\",\"data\":\"" ++ taunt ++ "\"}"
connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main :: IO ()
main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey] -> do
      run server port botname botkey
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
      exitFailure

run :: HostName -> String -> String -> String -> IO ()
run server port botname botkey = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  hPutStrLn h $ messageJoin botname botkey
--  hPutStrLn h $ messageCreateRace botname botkey "usa"
{-
 - Available tracks:
 - germany
 - keimola
 - usa
 -}
  global_game_state <- newIORef undefinedGameState
  handleMessages global_game_state h
  return ()

handleMessages :: IORef GameState -> Handle -> IO ()
handleMessages global_game_state h = do
  msg <- hGetLine h
  case decode (L.pack msg) of
    Just json ->
      let decoded = fromJSON json >>= decodeMessage in
      case decoded of
        Success serverMessage -> handleServerMessage global_game_state h serverMessage
        Error s -> fail $ "Error decoding message: " ++ s
    Nothing -> do
      fail $ "Error parsing JSON: " ++ (show msg)
  return ()

data ServerMessage =
    Join
  | GameInit GameInitData
  | GameEnd GameEndData
  | TournamentEnd
  | CarPositions [CarPosition] Int
  | YourCar CarId
  | GameStart
  | CarCrashed CarId
  | CarSpawned CarId
  | BotError
  | LapFinished
  | Finish
  | DidNotFinish
  | Turbo TurboData
  | Unknown String

handleServerMessage :: IORef GameState -> Handle -> ServerMessage -> IO ()
handleServerMessage global_game_state h serverMessage = do
  responses <- respond global_game_state serverMessage
  forM_ responses $ hPutStrLn h
  handleMessages global_game_state h
  return ()

respond :: IORef GameState -> ServerMessage -> IO [ClientMessage]
respond global_game_state message = case message of
  Join -> do
    putStrLn "Joined"
    return [messagePing]
  GameInit gameInit -> do
    putStrLn $ "GameInit: " ++ (reportGameInit gameInit)
    ref <- readIORef global_game_state
    writeIORef global_game_state $ ref { gameInitData = gameInit }
    return [messagePing]
  GameEnd gameEnd -> do
    putStrLn "Game ended"
    return [messagePing]
  TournamentEnd -> do
    putStrLn "Tournament ended"
    return [messagePing]
  CarPositions cps tick -> do
    ref <- readIORef global_game_state
    let news = current_speed $ mkSpeedInfo cps tick ref
        gs' = ref { carPositions = cps, gameTick = tick, speed = news }
    --putStrLn $ "tick = " ++ show tick ++ ", speed = " ++ show news
    writeIORef global_game_state gs'
    let allPieces = pieces . track . race . gameInitData $ gs'
    let numPieces = length allPieces
    let myPos = head . filter (\x -> CarPositionsModel.carId x == myCar gs') $ carPositions gs'
    let nextPiece = (1 + (pieceIndex . piecePosition $ myPos)) `mod` numPieces
    let secondPiece = (2 + (pieceIndex . piecePosition $ myPos)) `mod` numPieces
    let thirdPiece = (3 + (pieceIndex . piecePosition $ myPos)) `mod` numPieces
    let troubleAhead
          = or [ and [ news > 5
                     , maybe False (> 10) (pieceAngle $ allPieces !! nextPiece)
                     ]
               , CarPositionsModel.angle myPos > 5
               ]
    let throttle = if troubleAhead then 0.01 else 0.6
    return $ [messageThrottle throttle]
  YourCar yourCar -> do
    let ycp = CarPosition yourCar 0 $ PiecePosition 0 0 (CarLane 0 0) 0
    putStrLn $ "YourCar: " ++ (show yourCar)
    ref <- readIORef global_game_state
    writeIORef global_game_state $ ref { myCar = yourCar, carPositions = [ycp],
                                         gameTick = 0, speed = 0 }
    return [messagePing]
  GameStart -> do
    putStrLn "Started"
    return [messagePing]
  CarCrashed crashedCar -> do
    putStrLn "A car crashed" -- Check if it's our car.
    return [messagePing]
  CarSpawned spawnedCar -> do
    putStrLn "A car spawned" -- Check if it's our car.
    return [messagePing]
  BotError -> do
    return [messagePing] -- We're out.
  LapFinished -> do
    putStrLn "Lap finished"
    return [messagePing]
  Finish -> do
    putStrLn "Car finished"
    return [messagePing]
  DidNotFinish -> do
    putStrLn "Car disconnected"
    return [messagePing]
  Turbo turbo -> do
    ref <- readIORef global_game_state
    let allPieces = pieces . track . race . gameInitData $ ref
    let numPieces = length allPieces
    let myPos = head . filter (\x -> CarPositionsModel.carId x == myCar ref) $ carPositions ref
    let nextPiece = (1 + (pieceIndex . piecePosition $ myPos)) `mod` numPieces
    let nextSecondPiece = (2 + (pieceIndex . piecePosition $ myPos)) `mod` numPieces
    let news = current_speed $ mkSpeedInfo (carPositions ref) (gameTick ref) ref
    let troubleAhead
          = and [ news < 10 / (turboFactor turbo)
                , maybe False (> 1) (pieceAngle $ allPieces !! nextPiece)
                , maybe False (> 5) (pieceAngle $ allPieces !! nextSecondPiece)
                , False
                ]
    let message = if troubleAhead then messagePing else messageTurbo (show (nextPiece, nextSecondPiece))
    putStrLn $ "Turbo available " ++ (show turbo)
    return [message]
    --return [messagePing]
    --return [messageTurbo "LOLPWND"]
  Unknown msgType -> do
    putStrLn $ "Unknown message: " ++ msgType
    return [messagePing]

decodeMessage :: (String, Value, Maybe String, Maybe Int) -> Result ServerMessage
decodeMessage (msgType, msgData, msgGameId, msgGameTick)
  | msgType == "join" = Success Join
  | msgType == "gameInit" = GameInit <$> (fromJSON msgData)
  | msgType == "gameStart" = Success GameStart
  | msgType == "gameEnd" = GameEnd <$> (fromJSON msgData)
  | msgType == "tournamentEnd" = Success TournamentEnd
  | msgType == "yourCar" = YourCar <$> (fromJSON msgData)
  | msgType == "carPositions" = case msgGameTick of
    Just t -> (`CarPositions` t) <$> (fromJSON msgData)
    Nothing -> (`CarPositions` (-1)) <$> (fromJSON msgData)
  | msgType == "crash" = CarCrashed <$> (fromJSON msgData)
  | msgType == "spawn" = CarSpawned <$> (fromJSON msgData)
  | msgType == "error" = Success BotError
  | msgType == "lapFinished" = Success LapFinished
  | msgType == "finish" = Success Finish
  | msgType == "dnf" = Success DidNotFinish
  | msgType == "turboAvailable" = Turbo <$> (fromJSON msgData)
  | otherwise = Success $ Unknown msgType

instance FromJSON a => FromJSON (String, a, Maybe String, Maybe Int) where
  parseJSON (Object v) = do
    msgType <- v .: "msgType"
    msgData <- v .: "data"
    msgGameId <- v .:? "gameId"
    msgGameTick <- v .:? "gameTick"
    return (msgType, msgData, msgGameId, msgGameTick)
  parseJSON x          = fail $ "Not an JSON object: " ++ (show x)

