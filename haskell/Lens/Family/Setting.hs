module Lens.Family.Setting where

import Control.Applicative (Applicative, pure, (<*>))

newtype Setting a = Setting { unSetting :: a }

instance Functor Setting where
  fmap f (Setting a) = Setting (f a)

instance Applicative Setting where
  pure = Setting
  Setting f <*> Setting a = Setting (f a)
